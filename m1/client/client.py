# Open IEDCS client
import random

from init import init
from criptography import *
import string
from gui import read_epub

# open_file: opens the file specified in file_in

def open_file(file_in):
    try:
        fileKey = gen_file_key('IEDCSTEST2')

        ebook = decrypt_file(fileKey, file_in)
    except Exception:
        print 'This ebook was not purchased for this user.'
        return None

    read_epub(ebook)

# download_file: retreives file from server
def download_file(fileid):
    temp = library_path
    temp += ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(5))
    temp += '.edcs'

    alias = get_client_alias()

    data = {'clientid': alias, 'filename': fileid}
    url = server_url + '/download_file/'

    f = requests.get(url, data=json.dumps(data), auth=fileauth, headers=headers, verify=crt)
    if str(f) == '<Response [401]>':
        return None

    # Open our local file for writing
    with open(temp, "wb") as local_file:
        for chunk in f.iter_content(chunk_size=1024):
            if chunk:
                local_file.write(chunk)

    return temp

def purchase_file(fileid):
    alias = get_client_alias()

    url = server_url + '/purchase_file/'
    data = {'clientid': alias, 'filename': fileid}

    if requests.get(url, data=json.dumps(data), auth=fileauth, headers=headers, verify=crt).content == 'OK':
        return True
    else:
        return False

# Validate player
if not init():
    print "Unable to validate this player instance."
    print "Either this IEDCS installation is invalid or the IEDCS server is offline."
    os._exit(1)

file_to_get = 'IEDCSTEST2'


print 'Now trying to purchase a sample ebook from the server...'
purchase = purchase_file(file_to_get)
if (purchase):
    print 'Purchase succeeded!'
else:
    print 'Purchase failed! Either you already have purchased this title or the requested file is no longer available.'

print 'Now downloading sample ebook file from the server...'
tmpf = download_file(file_to_get)

print 'Opening downloaded ebook...'
if tmpf != None:
    open_file(tmpf)
else:
    print 'Failed to open file'

print 'Cleaning up downloaded file...'
os.unlink(tmpf)