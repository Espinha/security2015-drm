# Client initialization functions

from Crypto.Hash import SHA256
from install import gen_device_key, generate_alias
from criptography import *
from config import *
import json
import base64
import sql

# Start application
def init():
    # load previous configuration, if it exists
    if os.path.exists(alias_path) and os.path.exists(device_key_path):
        print 'Welcome to IEDCS Player!'
        read_config()
        return True

    else:
        print 'Welcome to IEDCS Player!\n'
        print 'Performing install...\n'
        if(test_conn()):
            create_config()

            # Send device key to server
            valid = send_device_key()
            if not valid:
                os.unlink(alias_path)
                os.unlink(device_key_path)
                os.unlink(database)
            return valid

        return False


# validate the installation
def test_conn():
    test_url = server_url + '/hello'
    return requests.post(test_url, data='w7_4A#?B/!Lepv<;', headers=headers, auth=basicauth, verify=crt).content == 'OK'

def send_device_key():

    player_key = get_player_key()
    device_key = get_dev_key()
    username = get_user_name()
    alias = get_client_alias()

    device_url = server_url + '/hello2'
    ciph_dev = encrypt(player_key, device_key)
    c= base64.b64encode(ciph_dev)

    payload = {'username': username, 'alias': alias, 'key': c}

    return requests.post(device_url, data =json.dumps(payload), auth=keyauth, headers=headers, verify=crt).content == 'OK'


# TODO: Rewrite this code
def read_config():

    set_player_key(SHA256.new('ID_PL_18272010').digest())

    with open(alias_path, 'rb') as config:
        client_alias = config.read()
    config.close()

    with open(device_key_path, 'rb') as config:
        device_key = config.read()
    config.close()

    set_dev_key(device_key)
    set_client_alias(client_alias)

def create_config():
    set_player_key(SHA256.new('ID_PL_18272010').digest())

    device_key = gen_device_key()
    device_alias = generate_alias()
    device_alias = base64.b64encode(device_alias)

    with open(alias_path, 'wb') as config:
        config.write(device_alias)
    config.close()

    with open(device_key_path, 'wb') as config:
        config.write(device_key)
    config.close()

    sql.init_sql()

    set_dev_key(device_key)
    set_client_alias(device_alias)

