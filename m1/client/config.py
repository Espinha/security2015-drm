import os
from requests.auth import HTTPBasicAuth

# Player Key, Device Key and client alias
player_key = None
device_key = None
client_alias = None

# Variables definition
server_url = 'https://localhost:8000'
security_folder = os.path.dirname(os.path.realpath(__file__)) + '/security/'
library_path = os.path.dirname(os.path.realpath(__file__)) + '/library/'

database = security_folder + 'iedcs.db'

# Auth types

basicauth = HTTPBasicAuth('iedcsp^kYQF!', 'a3tGR4ULq_p=w@Sd')
keyauth = HTTPBasicAuth('iedcskKRKm', '5ujCjm8TwGBG*G$b')
fileauth = HTTPBasicAuth('iedcsf$NhpS^', '8PAxd$_?UuXKJYJk')

# Static username and password for now
username = 'username1'
password = 'password1'

# Headers
headers = {'user-agent': 'iedcs-player/0.1'}

crt = security_folder + 'iedcs.crt'

device_key_path = security_folder + 'device.key'
alias_path = security_folder + 'iedcs.cfg'

def set_player_key(value):
    global player_key
    player_key = value

def set_dev_key(value):
    global device_key
    device_key = value

def get_player_key():
    return player_key

def get_dev_key():
    return device_key

def get_user_name():
    return username

def set_user_name(value):
    global username
    username = value

def get_client_alias():
    return client_alias

def set_client_alias(value):
    global client_alias
    client_alias = value