# server.py
# Handles every server request

from config import *
from cryptography import *
import string
import random
import json


# Routes

@app.route('/file-iv/', methods=['GET'])
@valid_header
@key_auth
def get_file_iv():
    data = json.loads(request.data)

    clientid = data['clientid']
    filename = data['filename']

    fileiv = get_file_iv_db(filename, clientid)
    if fileiv == -1:
        return Response('Unauthorized request', 401)
    fileiv = base64.b64encode(fileiv)
    return fileiv

# basic challenge
@app.route('/hello', methods=['POST'])
@basic_auth
@valid_header
def greet():
    # secret passphrase
    if request.data == 'w7_4A#?B/!Lepv<;':
        return 'OK'
    else:
        return Response('Unauthorized request', 401)

@app.route('/hello2', methods=['POST'])
@key_auth
@valid_header
def newDevice():

    try:
        data = json.loads(request.data)

        # payload = {'username': username, 'alias': alias, 'key': c}
        username = data['username']
        alias = data['alias']
    except Exception:
        return Response('Unauthorized request', 401)

    player_key = get_player_key()

    # get the original device key
    device_key = data['key']
    device_key = base64.b64decode(device_key)
    device_key = decrypt(player_key, device_key)

    if new_device(username, alias, device_key):
        return 'OK'

    return Response('Unauthorized request', 401)

# encrypt message with user key
@app.route('/encrypt/', methods=['GET', 'POST'])
@key_auth
@valid_header
def enc_ukey():
    if request.method == 'POST':
        try:
            data = json.loads(request.data)

            # payload = {'username': username, 'alias': alias, 'key': c}
            alias = data['clientid']
            message = data['message']

            message = base64.b64decode(message)
        except Exception:
            return Response('Unauthorized request', 401)

        # get user key
        user_data = get_user_data(alias)
        user_id = user_data[0][0]
        user_id = str(user_id)
        user_key = get_user_key(user_id)

        return encrypt(user_key, message)


# /decukey
@app.route('/decukey', methods=['GET', 'POST'])
#@requires_auth
@valid_header
def dec_ukey():
    if request.method == 'POST':
        return decrypt(user_key, request.data)

# Method for purchasing a file
@app.route('/purchase_file/', methods=['GET'])
@file_auth
@valid_header
def purchase_f():

    data = json.loads(request.data)

    alias = data['clientid']
    filename = data['filename']

    process_purchase(alias, filename)

    return 'OK'

# Method for downloading a purchased file
@app.route('/download_file/', methods=['GET'])
@file_auth
@valid_header
def download_stage1():
    try:
        data = json.loads(request.data)

        alias = data['clientid']
        filename = data['filename']
    except Exception:
        return Response('Unauthorized request', 401)

    iv = get_file_iv_db(filename, alias)
    if iv == -1:
        return Response('Unauthorized request', 401)


    fileKey = get_file_key(filename, alias)

    temp = app.config['TEMP'] + ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(5))
    temp = temp + '.enc'

    filename = app.config['FILES']+get_file_path(filename)

    encrypt_file(fileKey, filename, temp)

    toreturn = send_file(temp, as_attachment=True)

    os.unlink(temp)

    return toreturn


if __name__ == '__main__':
    app.run(host='localhost', port=8000, ssl_context=app.config['CONTEXT'])