import base64
from Crypto.Hash import SHA256
import config

# Init tables
config.insertRows('crypto.db', 'files', (None, 'IEDCSTEST1', 'readme.txt'), 1)
config.insertRows('crypto.db', 'files', (None, 'IEDCSTEST2', 'test.epub'), 1)

player_key = SHA256.new('ID_PL_18272010').digest()
player_key = config.encrypt(config.master_key, player_key)
player_key = base64.b64encode(player_key)

config.insertRows('crypto.db', 'players', (None, player_key, 0), 1)